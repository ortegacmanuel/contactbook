Class {
	#name : #WAContact,
	#superclass : #WAComponent,
	#instVars : [
		'contact'
	],
	#category : #'ContactBook-Components'
}

{ #category : #'as yet unclassified' }
WAContact class >> editContact: aContact [

	^ self new
		setContact: aContact;
		yourself
]

{ #category : #initialization }
WAContact >> contact [

	^ contact
]

{ #category : #initialization }
WAContact >> initialize [

	super initialize.
	contact := Contact new.
]

{ #category : #rendering }
WAContact >> renderButtonsOn: html [

	html formGroup: [
		html buttonGroup: [
			self
				renderSubmitButtonOn: html;
				renderCancelButtonOn: html ] ]

	
]

{ #category : #rendering }
WAContact >> renderCancelButtonOn: html [

	html formButton
		beDanger;
		cancelCallback: [ self answer: nil ];
		with: 'Cancel'
]

{ #category : #initialization }
WAContact >> renderContentOn: html [

	html container: [
		html heading with: 'Contact Editing'.
		html form with: [
			self renderFieldsOn: html.
			self renderButtonsOn: html ] ]
]

{ #category : #rendering }
WAContact >> renderEmailFieldOn: html [

	html formGroup: [
		html label: 'Email'.
		html emailInput
			formControl;
			placeholder: 'your@email.eu';
			callback: [ :email | self contact email: email address ];
			value: (self contact email ifNil: '') ]
]

{ #category : #rendering }
WAContact >> renderFieldsOn: html [

	self renderFullnameFieldOn: html.
	self renderEmailFieldOn: html
]

{ #category : #rendering }
WAContact >> renderFullnameFieldOn: html [

	html formGroup: [
		html label: 'Fullname'.
		html textInput
			formControl;
			placeholder: 'fullname';
			callback: [ :value | self contact fullname: value ];
			value: (self contact fullname ifNil: '') ]
]

{ #category : #rendering }
WAContact >> renderSubmitButtonOn: html [

	html formButton
		beSuccess;
		bePrimary;
		callback: [ self answer: self contact ];
		with: 'Save'
]

{ #category : #rendering }
WAContact >> rendererClass [
	
	^ SBSHtmlCanvas 
]

{ #category : #initialization }
WAContact >> setContact: aContact [

	contact := aContact
]
