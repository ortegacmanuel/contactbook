Class {
	#name : #WAContactBook,
	#superclass : #WAComponent,
	#instVars : [
		'contactBook'
	],
	#category : #'ContactBook-Components'
}

{ #category : #initialization }
WAContactBook class >> initialize [

	(WAAdmin register: self asApplicationAt: 'contacts')
		addLibrary: JQDevelopmentLibrary;
		addLibrary: SBSDevelopmentLibrary
]

{ #category : #adding }
WAContactBook >> addContact [

	(self call: WAContact new)
		ifNotNil: [ :contact | contactBook addContact: contact ]
]

{ #category : #accessing }
WAContactBook >> contactBook [

	^ contactBook ifNil: [ contactBook := ContactBook createDefault ]
]

{ #category : #accessing }
WAContactBook >> contacts [

	^ self contactBook contacts
]

{ #category : #accessing }
WAContactBook >> contactsDo: aBlock [

	self contacts do: aBlock
]

{ #category : #rendering }
WAContactBook >> renderButtonsForContact: aContact on: html [

	html buttonGroup: [
		self
			renderEditButtonForContact: aContact on: html;
			renderRemoveButtonForContact: aContact on: html ]
]

{ #category : #rendering }
WAContactBook >> renderContact: aContact on: html [

	html tableRow: [
		html tableData: aContact fullname;
		tableData: aContact email;
		tableData: [ self renderPhotoOf: aContact on: html ];
		tableData: [ self renderButtonsForContact: aContact on: html ]]
]

{ #category : #rendering }
WAContactBook >> renderContactsOn: html [

	html table 
		class: 'table';
		with: [
					html tableHead: [
						html tableHeading: 'Name';
						tableHeading: 'Email';
						tableHeading: 'Photo';
						tableHeading: 'Actions' ].
					self contactsDo: [ :contact | self renderContact: contact on: html ]]
]

{ #category : #rendering }
WAContactBook >> renderContentOn: html [
	"Main entry point of the view. Render a title."
	
	html
		container: [
			html heading
				level: 1;
				with: 'My Contact Book'.
			html form: [ 
				self renderContactsOn: html.
				self renderGlobalButtonsOn: html ]]
]

{ #category : #rendering }
WAContactBook >> renderEditButtonForContact: aContact on: html [

	html formButton
		beSuccess;
		callback: [ self call: (WAContact editContact: aContact) ];
		with: 'Edit'
]

{ #category : #rendering }
WAContactBook >> renderGlobalButtonsOn: html [

	html buttonGroup: [
		html formButton
			beSuccess;
			callback: [ self addContact ];
			with: 'New contact' ]
]

{ #category : #rendering }
WAContactBook >> renderPhotoOf: aContact on: html [

	html image url: aContact gravatarUrl
]

{ #category : #rendering }
WAContactBook >> renderRemoveButtonForContact: aContact on: html [

	html formButton
		beDanger;
		callback: [ self contactBook removeContact: aContact ];
		with: 'Remove'
]

{ #category : #rendering }
WAContactBook >> rendererClass [
	
	^ SBSHtmlCanvas 
]

{ #category : #updating }
WAContactBook >> updateRoot: anHtmlRoot [

	super updateRoot: anHtmlRoot.
	anHtmlRoot title: 'Contact Book'
]
