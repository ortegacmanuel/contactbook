Class {
	#name : #ContactTest,
	#superclass : #TestCase,
	#category : #'ContactBook-Tests'
}

{ #category : #tests }
ContactTest >> testCreation [

	|contact|
	contact := Contact newNamed: 'Markus Denker' email: 'marcus.denker@inria.fr'.
	self assert: contact fullname equals: 'Markus Denker'.
	self assert: contact email equals: 'marcus.denker@inria.fr'
]
